export default {
  state: {
    step: 1,
    phone: '',
    code: '',
    password: '',
    user: false,
  },
  actions: {
    registrationPhoneCheck({ commit }, { phone }) {
      commit('registrationPhoneCheck', { phone });
    },
    codeCheck({ commit }, { phone, code }) {
      commit('codeCheck', { phone, code });
    },
    registrationUser({ commit }, { password, phone }) {
      commit('registrationUser', {
        password,
        phone,
      });
    },
    loginUser({ commit }, { phone, password }) {
      commit('loginUser', { phone, password });
    },
  },
  mutations: {
    registrationPhoneCheck(state, { phone }) {
      state.phone = phone;
      state.step++;
    },
    codeCheck(state, { code }) {
      state.code = code;
      state.step++;
    },
    registrationUser(state, { password, phone }) {
      state.password = password;
      state.phone = phone;
      state.user = true;
    },
    loginUser(state, { phone, password }) {
      state.phone = phone;
      state.password = password;
      state.user = true;
    },
  },
  getters: {
    stepLoading(state) {
      return state.step;
    },
    isUserLoggedIn(state) {
      return state.user;
    },
  },
};
