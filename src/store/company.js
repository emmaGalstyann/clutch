export default {
  state: {
    company: [],
  },
  actions: {
    setCompany({ commit }, payload) {
      commit('setCompany', payload);
    },
    createCompany({ commit }, payload) {
      commit('createCompany', payload);
    },
  },
  mutations: {
    setCompany(state, payload) {
      state.company = payload;
    },
    createCompany(state, payload) {
      state.company.push(payload);
    },
  },
  getters: {
    company(state) {
      return state.company;
    },
  },
};
