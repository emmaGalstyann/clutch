import Vue from 'vue';
import Vuex from 'vuex';

import user from './user';
import company from './company';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    company,
  },
});
