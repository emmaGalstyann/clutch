import Vue from 'vue';
import Router from 'vue-router';
import AuthGuard from './auth-quard';
import Company from '../components/adminPages/Company';
import AuthorizationPage from '../components/auth/AuthorizationPage';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/company',
      name: 'company',
      component: Company,
      beforeEnter: AuthGuard,
    },
    {
      path: '/',
      name: 'login',
      component: AuthorizationPage,
    },
  ],
});
