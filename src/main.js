import Vue from 'vue';

import router from './router/';
import store from './store/';

import axios from 'axios';
import VueAxios from 'vue-axios';

import Vuelidate from 'vuelidate';
import vuetify from './plugins/vuetify';

import App from './App.vue';

import './assets/style.scss';

Vue.use(VueAxios, axios);
Vue.use(Vuelidate);

Vue.config.productionTip = false;

new Vue({
  router,
  axios,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');
